import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Adelin on 24-May-17.
 */
public class Controller {
    public List<MonitoredData> data = new ArrayList();

    public List<String> test() {
        Map<String, Map<Integer, Long>> collect = data.stream()
                .collect(Collectors.groupingBy((MonitoredData m) -> m.getActivityLabel(),
                        Collectors.groupingBy((MonitoredData m) -> m.getStartTime().getDayOfMonth(), Collectors.counting())));

        return collect.entrySet().stream()
                .filter(m->m.getValue().size()>distinctDays()*0.9)
                .map(m->m.getKey())
                .collect(Collectors.toList());
    }


    public long distinctDays(){
        return data.stream()
                .map(monitoredData->monitoredData.endTime.getDayOfMonth())
                .distinct()
                .count();
    }

    public Map distinctAction(){
        return data.stream()
                .collect(Collectors.groupingBy((MonitoredData m )->m.getActivityLabel(),Collectors.counting()));
    }

    public Map activitiesEachDay(){
        return data.stream()
                .collect(Collectors.groupingBy((MonitoredData m) ->m.getStartTime().getDayOfYear(),
                        Collectors.groupingBy((MonitoredData m)->m.getActivityLabel(),Collectors.counting())));

    }

    public Map activitiesDuration(){
        Map<String,Long> map = data.stream()
                .collect(Collectors.toMap((MonitoredData m )-> m.getActivityLabel(),
                        value-> ChronoUnit.HOURS.between(value.getStartTime().toInstant(ZoneOffset.UTC),
                                value.getEndTime().toInstant(ZoneOffset.UTC)),(p1, p2)->p2+p1));
        return map.entrySet().stream()
                .filter(m->m.getValue()>10)
                .collect(Collectors.toMap(p->p.getKey(),p->p.getValue()));
    }

    public void loadInfo(){
        String fileName = "Activities.txt";

        try(Stream<String> stream = Files.lines(Paths.get(fileName))){

            stream.forEach(string->{
                DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String[] parts = string.split("\t\t");

                LocalDateTime start = LocalDateTime.parse(parts[0],format);
                LocalDateTime end = LocalDateTime.parse(parts[1],format);

                MonitoredData monitoredData = new MonitoredData();
                monitoredData.setActivityLabel(parts[2]);
                monitoredData.setEndTime(end);
                monitoredData.setStartTime(start);

                data.add(monitoredData);
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void saveInfo(Map m, String fileName){
        try{
            PrintWriter writer = new PrintWriter(fileName);
            writer.println(m);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
