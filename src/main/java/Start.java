
/**
 * Created by Adelin on 24-May-17.
 */
public class Start {
    public static void main(String args[]){
        Controller controller = new Controller();
        controller.loadInfo();

        System.out.println("1. Distinct days:");
        System.out.println(controller.distinctDays());

        System.out.println("2. Counting Distinct Actions:");
        System.out.println(controller.distinctAction());
        controller.saveInfo(controller.distinctAction(),"DistinctActions.txt");

        System.out.println("3.Activities each day:");
        System.out.println(controller.activitiesEachDay());
        controller.saveInfo(controller.activitiesEachDay(),"ActivitiesEachDay.txt");

        System.out.println("4.Activities duration:");
        System.out.println(controller.activitiesDuration());
        controller.saveInfo(controller.distinctAction(),"ActivitiesDuration.txt");

        System.out.println(controller.test());

    }
}