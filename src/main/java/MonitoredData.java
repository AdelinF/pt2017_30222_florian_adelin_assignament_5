import java.time.LocalDateTime;
/**
 * Created by Adelin on 24-May-17.
 */
public class MonitoredData {
    public String activityLabel;
    public LocalDateTime startTime;
    public LocalDateTime endTime;

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
